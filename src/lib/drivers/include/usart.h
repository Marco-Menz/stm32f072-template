/**
 * @file
 *****************************************************************************
 * @title   uart.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   Uart functions
 *******************************************************************************/

#ifndef _IOC_UART_H_
#define _IOC_UART_H_

#ifdef _cplusplus
extern "C"
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"

/* Function declarations -----------------------------------------------------*/

bool usart1_init(void);
xQueueHandle usart1_get_rx_queue_handle(void);
int usart1_getc(void);
void usart1_putc(char c);

#ifdef _cplusplus
}
#endif

#endif  /* _IOC_UART_H_ */

/* EOF */
