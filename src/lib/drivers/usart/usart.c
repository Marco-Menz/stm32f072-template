/**
 * @file
 *****************************************************************************
 * @title   uart.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   Uart functions
 *******************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include "stm32f0xx.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_usart.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "usart.h"
#include "printf.h"
#include "conversions.h"

/* Globals */

#define BINGO       do { } while(true)
#define UART_MSG_SIZE            1
#define UART_MSG_QUEUE_ENTRIES   10

static xQueueHandle uart1_queue = NULL;

/* Function declarations -----------------------------------------------------*/

/**
 * @brief  Retargets the C library putc function to the USART.
 * @param  p    Ignored, could be used to buffer into memory
 * @param  c    Character value to print
 * @retval None
 */
void our_putc(void* p P_UNUSED, char c, sprintf_state_t* state)
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE ) == RESET)
        ;
    if ((state->buf_left == -1) || (state->buf_left-- > 0))
    {
        USART_SendData(USART1, (uint16_t) c);
    }
}

void usart1_putc(char c)
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE ) == RESET)
        ;
    USART_SendData(USART1, (uint16_t) c);
}


/**
 * Returns queue handle where usart ISR would write received
 * character to. A consumer calls usart1_getc() to receive
 * this character.
 *
 * @return rx queue handle
 *
 * @see usart1_getc
 */
xQueueHandle usart1_get_rx_queue_handle(void)
{
    return uart1_queue;
}


/**
 * @brief  This function handles USART1 global interrupt request.
 * @param  None
 * @retval None
 *
 * @note name must not be altered as it is referenced inside startup code
 */
void USART1_IRQHandler(void)
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE ) != RESET)
    {
        {
            //uint8_t ReadByte = (USART_ReceiveData(USART1 ) & 0x7F);
            uint8_t ReadByte = USART_ReceiveData(USART1 );

            /* The echo of this character should be done where it will be
             * consumed. The consumer can decide if it should be echoed
             * or suppressed.
             */

            portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
            xQueueSendFromISR(uart1_queue, &ReadByte,
                    &xHigherPriorityTaskWoken);
            if (xHigherPriorityTaskWoken)
            {
                taskYIELD();
            }
        }
    }
}

/**
 * Returns from USART1 IRQ sent bytes
 */
int usart1_getc(void)
{
    int ch;
    while (pdTRUE != xQueueReceive(uart1_queue, &ch, 1000))
    {
        ;   // wait endlessly
    }
    return ch;
}

/**
 * @brief  Configures the nested vectored interrupt controller.
 * @param  None
 * @retval None
 */
void usart_nvic_init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable the USART1 Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief  Configures COM port.
 * @retval None
 */
void usart_main_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl =
            USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    /* Connect PXx to USARTx_Tx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1 );

    /* Connect PXx to USARTx_Rx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1 );

    /* Configure USART Tx, Rx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* USART configuration */
    USART_Init(USART1, &USART_InitStructure);

    /* Enable USART */
    USART_Cmd(USART1, ENABLE);
    init_printf(NULL, our_putc);
}

/**
 * @brief initializes the uart
 * @see usart_nvic_init() usart_main_init() USART_ITConfig() xQueueCreate()
 */
bool usart1_init(void)
{
    /*!< At this stage the microcontroller clock setting is already configured,
     this is done through SystemInit() function which is called from startup
     file (startup_stm32f0xx.s) before to branch to application main.
     To reconfigure the default setting of SystemInit() function, refer to
     system_stm32f0xx.c file
     */

    /* NVIC configuration */
    usart_nvic_init();

    /* USARTx configuration ------------------------------------------------------*/
    usart_main_init();

    /* Enable the USART1 Transmit interrupt: this interrupt is generated when the
     USART1 transmit data register is empty */
    USART_ITConfig(USART1, USART_IT_TXE, DISABLE);

    /* Enable the USART1 Receive interrupt: this interrupt is generated when the
     USART1 receive data register is not empty */
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

    uart1_queue = xQueueCreate(UART_MSG_QUEUE_ENTRIES, UART_MSG_SIZE);
    if (uart1_queue == 0)
    {
        BINGO;
        return false;
    }
    return true;
}

/* EOF */

