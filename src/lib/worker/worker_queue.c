#include "worker_queue.h"

static xQueueHandle worker_comm_queue = NULL;

/**
 * @brief worker task communication queue size
 */
#define  WORKER_QUEUE_LEN 10

 xQueueHandle worker_comm_get_hdl(void)
 {
	 if(NULL == worker_comm_queue )
	 {
		 worker_comm_queue = xQueueCreate( WORKER_QUEUE_LEN, sizeof( Worker_Message_t ) );
	 }
	 return worker_comm_queue;
 }
