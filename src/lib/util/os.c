/**
 * @file
 *****************************************************************************
 * @title   os.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   Logging implementation to common logger task
 *******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "os.h"


/* Implementation -------------------------------------------------------------*/


typedef struct MON_TCB_NODE {

    xTaskHandle            tcb;        /* handle to freertos tcb */
    struct MON_TCB_NODE*    next;       /* next element in list */
} MON_TCB_NODE_T;

static MON_TCB_NODE_T* tcb_begin=NULL;



void os_add_tcb(xTaskHandle tcb)
{
    if (tcb)
    {
        MON_TCB_NODE_T* node = pvPortMalloc(sizeof(*tcb));
        node->next = NULL;
        node->tcb = tcb;

        if (NULL == tcb_begin)
        {
            tcb_begin = node;
        }
        else
        {
            MON_TCB_NODE_T* ptr = tcb_begin;
            while (ptr->next != NULL )
            {
                ptr = ptr->next;
            }
            ptr->next = node;
        }
    }
}


xTaskHandle os_tcb_next(xTaskHandle tcb)
{
    MON_TCB_NODE_T* ptr = (MON_TCB_NODE_T*) tcb_begin;
    xTaskHandle* next_tcb = NULL;

    if ((NULL == tcb) && (tcb_begin))
    {
        next_tcb = tcb_begin->tcb;
    }
    else
    {
        while (ptr)
        {
            if ((ptr->tcb == tcb) && (ptr->next))
            {
                next_tcb = ptr->next->tcb;
                break;
            }
            ptr = ptr->next;
        }
    }

    return next_tcb;
}


/* EOF */

